#!/bin/bash

VERSION=0.1.0

echo "Compiling..."
cd appPhonegap

#cordova build --release
phonegap build --verbose --release

# Current time and dateDATE= date + "%T"
DATE="$(date +"%T")"

# Where the app is compiled
#APKPATH=/media/extra/dev/bhavana/appPhonegap/platforms/android/build/outputs/apk/android-release-unsigned.apk
APKPATH=$PWD"/platforms/android/build/outputs/apk/android-release-unsigned.apk"
# Where I want the new app
APKRELEASE_PATH=$PWD"/releases/"
APKRELEASE_NAME=retiro_meditacion-$VERSION-$DATE.apk
APKRELEASE_FULL_PATH=$APKRELEASE_PATH$APKRELEASE_NAME

#echo "Removing old $APKRELEASE_FULL_PATH"
#rm "$APKRELEASE_FULL_PATH"

echo "Signing..."

# Step 1: Sign your app with your private key using
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /media/x/dev/bhavana/lasangha.keystore_0 $APKPATH lasangha

# Step 2 (optional): Verify that your APK is signed.
jarsigner -verify -verbose -certs $APKPATH

# Step 3: Align the final APK package using
/media/x/dev/android-sdk-linux/build-tools/21.1.2/zipalign -v 4 $APKPATH $APKRELEASE_FULL_PATH

echo "Moving..."
cp $APKRELEASE_FULL_PATH /var/www/html/t/$APKRELEASE_NAME

echo "Done!"
