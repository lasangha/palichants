#!/bin/bash

echo "Compiling..."
cd paliChant

phonegap build --verbose

# Where the app is compiled
APKPATH=$PWD"/platforms/android/build/outputs/apk/android-debug.apk"
echo "Looking for compiled output in:"$APKPATH

echo "Moving..."
cp $APKPATH /var/www/html/t/paliChant-debug-dev.apk

echo "Done!"

