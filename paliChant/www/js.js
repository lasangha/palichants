/* This is for cordova */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
};

/* // end of cordova */
var basePath = '';
var myMedia = "";

function phoneGapPath(){
    console.log("Detecting phonegap path");
    var path = window.location.pathname;
    path = path.substr(path, path.length - 10);
    console.log("The phone path is: " + path);
    return 'file://' + path;
}

function changeSong(newSong){

    console.log("Changing the song to: " + newSong);

	// Show player
	$("#webAudioControls").show('slow');

    // @todo: check for android or others
    if(basePath !== ""){
        console.log("Playing in app: " + basePath + 'audios/' + newSong + '.mp3');

        //alert(myMediai);
        if(myMedia){
            myMedia.stop();
            //myMedia = false;
        }

        myMedia = new Media(basePath + 'audios/' + newSong + '.mp3',
                                 // success callback
                                 function(){
                                     console.log("playAudio():Audio Success");
                                 },
                                 // error callback
                                 function(err){
                                     console.log("playAudio():Audio Error: " + JSON.stringify(err));
                                 }
                                );
                                // Play audio
                                myMedia.play();
    }else{
        console.log("Not in an app");
        //var audio = new Audio(Cala_createAudioPath() + 'modules/bhavana/bell_1.mp3');
        //audio.play();

        var audio = document.getElementById('audios');
        var source = document.getElementById('audioSource');
        source.src = basePath + 'audios/' + newSong + ".mp3";
        console.log("Path: " + basePath + 'audios/' + newSong + ".mp3");

        audio.load(); //call this to just preload the audio without playing
        audio.play(); //call this to play the song right away
    }

    // Change the text
    console.log("Changing the text");
    var textSource = document.getElementById('texts');
    textSource.src = 'texts/' + newSong + ".html";
    return false;
}

/*
Use this to track the time in audios and move the text
var audio = document.getElementById('audios');
audio.addEventListener('timeupdate',function(){
    var currentTimeMs = audio.currentTime*1000;
    console.log(currentTimeMs);
},false);


*/

